#ifndef ZGAME_SYSTEM_BRIDGE
#define ZGAME_SYSTEM_BRIDGE

#include <stdbool.h>

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include "math3d.h"

#define PROCESS_VK_RESULT(vk_function_call)                              \
{                                                                        \
	VkResult vk_result = vk_function_call;                               \
	if (VK_SUCCESS != vk_result)                                         \
	{                                                                    \
		printf("%s:%d: VkResult: %d\n", __FILE__, __LINE__, vk_result);  \
		return false;                                                    \
	}                                                                    \
}

#define PROCESS_RESULT(function_call, message)             \
{                                                          \
	if (!function_call)                                    \
	{                                                      \
		printf("%s:%d: %s\n", __FILE__, __LINE__, message);\
		return false;                                      \
	}                                                      \
}

typedef struct Extensions
{
	const char **names;
	uint32_t count;

} Extensions;
typedef struct OperationQueueFamilies
{
	int graphics_family_idx;
	int compute_family_idx;
	int present_family_idx;

	bool use_same_family;

} OperationQueueFamilies;
typedef struct CommandBuffers
{
	VkCommandBuffer *data;
	uint32_t count;

} CommandBuffers;
typedef struct Color
{
	float red;
	float green;
	float blue;
	float alpha;

} Color;
typedef struct Vertex
{
	Vector4 position;
	Color color;

} Vertex;
typedef struct Vertices
{
	Vertex *data;
	uint32_t count;

} Vertices;
typedef struct Indices
{
	uint32_t *data;
	uint32_t count;

} Indices;
typedef struct Particle
{
	Vector4 position;
	Color color;

	float radius;
	float _unused0;
	float _unused1;
	float _unused2;

} Particle;
typedef struct Particles
{
	Particle *data;
	uint32_t count;

} Particles;
typedef struct SurfaceFormats
{
	VkSurfaceFormatKHR* data;
	uint32_t count;

} SurfaceFormats;
typedef struct PresentModes
{
	VkPresentModeKHR* data;
	uint32_t count;

} PresentModes;

bool setup_window_and_gpu();
void destroy_window_and_free_gpu();

void create_particles();
void destroy_particles();
void add_particle(float x, float y, float z);
void _destroy_memory_buffers();

bool render();

const VkDevice* system_bridge_get_device();
const SurfaceFormats* system_bridge_get_supported_surface_formats();
const VkSurfaceCapabilitiesKHR* system_bridge_get_surface_capabilities();
const VkSurfaceKHR* system_bridge_get_surface();
const GLFWwindow* system_bridge_get_window();
const VkPhysicalDeviceMemoryProperties* system_bridge_get_supported_memory_properties();

#endif
