#include <stdint.h>
#include "math3d.h"

typedef struct UniformData
{
	Matrix4x4 model;
	Matrix4x4 view;
	Matrix4x4 projection;

} UniformData;

void application_track_mouse_position(float x, float y);
void application_select_area(int mouse_button, int glfw_action);
void application_toggle_screen_rotation(int key, int glfw_action);

void application_init_scene();
void application_rotate_scene(float time_diff);
UniformData* application_render_scene(uint32_t width, uint32_t height);
