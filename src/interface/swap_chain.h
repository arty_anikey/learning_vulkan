#ifndef ZGAME_SWAP_CHAIN
#define ZGAME_SWAP_CHAIN

typedef struct SwapChainImages
{
	VkImage* data;
	uint32_t count;

} SwapChainImages;
typedef struct SwapChainImageViews
{
	VkImageView* data;
	uint32_t count;

} SwapChainImageViews;
typedef struct SwapChainFramebuffers
{
	VkFramebuffer* data;
	uint32_t count;

} SwapChainFramebuffers;

void swap_chain_destroy();
bool swap_chain_create();

const VkExtent2D* swap_chain_get_image_extent();
const VkRenderPass* swap_chain_get_render_pass();
const SwapChainImages* swap_chain_get_images();
const SwapChainFramebuffers* swap_chain_get_framebuffers();
const VkSwapchainKHR* swap_chain_get();

#endif
