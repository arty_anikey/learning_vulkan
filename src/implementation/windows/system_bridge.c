#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#include "system_bridge.h"
#include "math3d.h"
#include "application.h"
#include "swap_chain.h"

#define DEVICE_QUEUES_COUNT 3
#define GPU_DATA_BINDINGS_COUNT 4
#define PHYSICAL_DEVICE_EXTENSIONS_COUNT 1
#define VALIDATION_LAYERS_COUNT 1
#define DEFAULT_WINDOW_WIDTH 800
#define DEFAULT_WINDOW_HEIGHT 600

#define INITIAL_PARTICLES_COUNT 8
#define MAX_PARTICLES_COUNT 1000

/* Module state */

static GLFWwindow *_window = NULL;
static VkInstance _instance;
static Extensions _required_physical_device_extensions;
static VkSurfaceKHR _surface;
static VkSurfaceCapabilitiesKHR _surface_capabilities;
static VkPhysicalDeviceMemoryProperties _supported_memory_properties;
static SurfaceFormats _supported_surface_formats;
static PresentModes _supported_present_modes;
static VkPhysicalDevice _physical_device = VK_NULL_HANDLE;
static VkDevice _device;
static VkQueue _graphics_queue;
static VkQueue _compute_queue;
static VkQueue _present_queue;
static VkCommandPool _command_buffer_pool;
static CommandBuffers _command_buffers;
static uint32_t _memory_copy_command_buffer_idx;
static uint32_t _image_draw_command_buffers_begin_idx;
static uint32_t _compute_command_buffer_idx;
static VkDescriptorPool _descriptor_pool;
static VkDescriptorSetLayout _descriptor_set_layout;
static VkDescriptorSet _descriptor_set;
static VkPipelineLayout _graphics_pipeline_layout;
static VkPipeline _graphics_pipeline;
static VkShaderModule _vertex_shader;
static char *_vertex_shader_code;
static VkShaderModule _fragment_shader;
static char *_fragment_shader_code;
static VkShaderModule _compute_shader;
static char *_compute_shader_code;
static VkPipelineLayout _compute_pipeline_layout;
static VkPipeline _compute_pipeline;
static VkFence _compute_finished_fence;
static VkBuffer _host_uniform_data_buffer;
static VkDeviceMemory _host_uniform_data_buffer_memory;
static VkBuffer _device_uniform_data_buffer;
static VkDeviceMemory _device_uniform_data_buffer_memory;
static VkBuffer _device_vertex_buffer;
static VkDeviceMemory _device_vertex_buffer_memory;
static VkBuffer _device_index_buffer;
static VkDeviceMemory _device_index_buffer_memory;
static VkBuffer _host_index_buffer;
static VkDeviceMemory _host_index_buffer_memory;
static VkBuffer _device_particle_buffer;
static VkDeviceMemory _device_particle_buffer_memory;
static VkBuffer _host_particle_buffer;
static VkDeviceMemory _host_particle_buffer_memory;
static VkSemaphore _image_available;
static VkSemaphore _render_finished;
static OperationQueueFamilies _operation_queue_families = {
	.graphics_family_idx = -1,
	.compute_family_idx = -1,
	.present_family_idx = -1,

	.use_same_family = false,
};

static Vertices _vertices;
static Indices _indices;
static Particles _particles;

static bool _are_extensions_supported(uint32_t required_instance_extensions_count, char** required_instance_extensions)
{
	uint32_t supported_instance_extensions_count = 0;
	vkEnumerateInstanceExtensionProperties(NULL, &supported_instance_extensions_count, NULL);

	VkExtensionProperties* supported_instance_extensions = malloc(sizeof(VkExtensionProperties) * supported_instance_extensions_count);
	vkEnumerateInstanceExtensionProperties(NULL, &supported_instance_extensions_count, supported_instance_extensions);

	bool all_supported = true;

	for (uint32_t i = 0; i < required_instance_extensions_count; i += 1) {
		bool extension_supported = false;

		for (uint32_t j = 0; j < supported_instance_extensions_count; j += 1)
		{
			if (strcmp(supported_instance_extensions[j].extensionName, required_instance_extensions[i]) == 0) {
				extension_supported = true;
				break;
			}
		}

		if (!extension_supported)
		{
			printf("Required extension %s is not supported", required_instance_extensions[i]);
			all_supported = false;
			break;
		}
	}

	free(supported_instance_extensions);

	return all_supported;
}
static bool _are_validation_layers_supported(uint32_t required_layers_count, char** required_layers)
{
	uint32_t supported_layers_count;
	vkEnumerateInstanceLayerProperties(&supported_layers_count, NULL);

	VkLayerProperties* supported_layers = malloc(sizeof(VkLayerProperties) * supported_layers_count);
	vkEnumerateInstanceLayerProperties(&supported_layers_count, supported_layers);

	bool all_supported = true;

	for (uint32_t i = 0; i < required_layers_count; i += 1) {
		bool layer_supported = false;

		for (uint32_t j = 0; j < supported_layers_count; j += 1)
		{
			if (strcmp(supported_layers[j].layerName, required_layers[i]) == 0) {
				layer_supported = true;
				break;
			}
		}

		if (!layer_supported)
		{
			printf("Required validation layer %s is not supported", required_layers[i]);
			all_supported = false;
			break;
		}
	}

	free(supported_layers);

	return all_supported;
}
static bool _create_instance(uint32_t required_instance_extensions_count, char** required_instance_extensions)
{
	PROCESS_RESULT(_are_extensions_supported(
		required_instance_extensions_count,
		required_instance_extensions
	), "Unsupported extensions");

	uint32_t required_validation_layers_count = 1;
	char* required_validation_layers[1] = { "VK_LAYER_KHRONOS_validation" };

#ifdef _DEBUG
	PROCESS_RESULT(_are_validation_layers_supported(
		required_validation_layers_count,
		required_validation_layers
	), "Unsupported validation layers");
#endif

	VkApplicationInfo app_info = {
		.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
		.pApplicationName = "zGame",
		.applicationVersion = VK_MAKE_VERSION(1, 0, 0),
		.pEngineName = "zEngine",
		.engineVersion = VK_MAKE_VERSION(1, 0, 0),
		.apiVersion = VK_API_VERSION_1_3
	};

	VkInstanceCreateInfo create_info = {
		.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
		.pApplicationInfo = &app_info,
	};

	create_info.ppEnabledExtensionNames = required_instance_extensions;
	create_info.enabledExtensionCount = required_instance_extensions_count;

#ifdef _DEBUG
	create_info.enabledLayerCount = 1;
	create_info.ppEnabledLayerNames = required_validation_layers;
#endif

	PROCESS_VK_RESULT(vkCreateInstance(&create_info, NULL, &_instance));

	return true;
}

static VkResult _create_logical_device()
{
	float queuePriorities[DEVICE_QUEUES_COUNT] = { 1.0f, 0.5f, 0.0f };

	VkDeviceQueueCreateInfo queue_create_info = {
		.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
		.queueFamilyIndex = _operation_queue_families.graphics_family_idx,  // all queues in this family
		.queueCount = DEVICE_QUEUES_COUNT,
		.pQueuePriorities = queuePriorities,
	};

	VkPhysicalDeviceFeatures device_features;
	vkGetPhysicalDeviceFeatures(_physical_device, &device_features);

	VkDeviceCreateInfo create_info = {
		.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
		.pQueueCreateInfos = &queue_create_info,
		.queueCreateInfoCount = 1,
		.pEnabledFeatures = &device_features,
		.enabledExtensionCount = _required_physical_device_extensions.count,
		.ppEnabledExtensionNames = _required_physical_device_extensions.names,
	};

	return vkCreateDevice(_physical_device, &create_info, NULL, &_device);
}
static VkResult _create_descriptor_pool()
{
	VkDescriptorPoolSize uniform_buffer_size = {
		.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
		.descriptorCount = 1,
	};

	VkDescriptorPoolSize storage_buffer_size = {
		.type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
		.descriptorCount = 3,
	};

	VkDescriptorPoolSize pool_sizes[] = {
		uniform_buffer_size,
		storage_buffer_size
	};

	VkDescriptorPoolCreateInfo pool_info = {
		.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
		.poolSizeCount = 2,
		.pPoolSizes = pool_sizes,
		.maxSets = GPU_DATA_BINDINGS_COUNT,
	};

	return vkCreateDescriptorPool(_device, &pool_info, NULL, &_descriptor_pool);
}
static VkResult _create_descriptor_set_layout()
{
	VkDescriptorSetLayoutBinding verticies_layout_binding = {
		.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
		.stageFlags = VK_SHADER_STAGE_COMPUTE_BIT | VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT,
		.binding = 0,
		.descriptorCount = 1,
	};

	VkDescriptorSetLayoutBinding indices_layout_binding = {
		.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
		.stageFlags = VK_SHADER_STAGE_COMPUTE_BIT,
		.binding = 1,
		.descriptorCount = 1,
	};

	VkDescriptorSetLayoutBinding uniform_data_layout_binding = {
		.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
		.stageFlags = VK_SHADER_STAGE_COMPUTE_BIT | VK_SHADER_STAGE_VERTEX_BIT,
		.binding = 2,
		.descriptorCount = 1,
	};

	VkDescriptorSetLayoutBinding particles_layout_binding = {
		.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
		.stageFlags = VK_SHADER_STAGE_COMPUTE_BIT,
		.binding = 3,
		.descriptorCount = 1,
	};

	VkDescriptorSetLayoutBinding bindings[GPU_DATA_BINDINGS_COUNT] = {
		verticies_layout_binding,
		indices_layout_binding,
		uniform_data_layout_binding,
		particles_layout_binding,
	};

	VkDescriptorSetLayoutCreateInfo descriptor_set_layout_ci = {
		.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
		.pNext = NULL,
		.pBindings = bindings,
		.bindingCount = GPU_DATA_BINDINGS_COUNT,
	};

	return vkCreateDescriptorSetLayout(_device, &descriptor_set_layout_ci, NULL, &_descriptor_set_layout);
}
static VkResult _create_shader_module(VkShaderModule* shader_module, const char bin_shader_file_path[], char* shader_code)
{
	FILE* file_descriptor = fopen(bin_shader_file_path, "rb");

	if (!file_descriptor)
	{
		return false;
	}

	fseek(file_descriptor, 0L, SEEK_END);
	uint32_t code_size = ftell(file_descriptor);

	rewind(file_descriptor);

	shader_code = malloc(code_size);

	fread(shader_code, code_size, 1, file_descriptor);

	VkShaderModuleCreateInfo create_info = {
		.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
		.codeSize = code_size,
		.pCode = (const uint32_t*)shader_code,
	};

	return vkCreateShaderModule(_device, &create_info, NULL, shader_module);
}
static VkResult _create_compute_pipeline()
{
	PROCESS_VK_RESULT(_create_shader_module(&_compute_shader, "compute.spv", _compute_shader_code));

	VkPipelineShaderStageCreateInfo compute_shader_stage_ci = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
		.stage = VK_SHADER_STAGE_COMPUTE_BIT,
		.module = _compute_shader,
		.pName = "main",
	};

	VkPipelineLayoutCreateInfo pipeline_layout_ci = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
		.pNext = NULL,
		.setLayoutCount = 1,
		.pSetLayouts = &_descriptor_set_layout,
	};

	PROCESS_VK_RESULT(vkCreatePipelineLayout(_device, &pipeline_layout_ci, NULL, &_compute_pipeline_layout));

	VkComputePipelineCreateInfo pipeline_ci = {
		.sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO,
		.layout = _compute_pipeline_layout,
		.flags = 0,
		.stage = compute_shader_stage_ci,
	};

	return vkCreateComputePipelines(_device, VK_NULL_HANDLE, 1, &pipeline_ci, NULL, &_compute_pipeline);
}
static VkResult _create_graphics_pipeline()
{
	const VkExtent2D* _swap_chain_image_extent = swap_chain_get_image_extent();
	const VkRenderPass* _render_pass = swap_chain_get_render_pass();

	PROCESS_VK_RESULT(_create_shader_module(&_vertex_shader, "vertex.spv", _vertex_shader_code));
	PROCESS_VK_RESULT(_create_shader_module(&_fragment_shader, "fragment.spv", _fragment_shader_code));

	VkPipelineShaderStageCreateInfo vertex_shader_stage_ci = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
		.stage = VK_SHADER_STAGE_VERTEX_BIT,
		.module = _vertex_shader,
		.pName = "main",
	};

	VkPipelineShaderStageCreateInfo fragment_shader_stage_ci = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
		.stage = VK_SHADER_STAGE_FRAGMENT_BIT,
		.module = _fragment_shader,
		.pName = "main",
	};

	VkPipelineShaderStageCreateInfo shaderStages[2] = {
		vertex_shader_stage_ci,
		fragment_shader_stage_ci,
	};

	VkVertexInputBindingDescription binding_description = {
		.binding = 0,
		.stride = sizeof(Vertex),
		.inputRate = VK_VERTEX_INPUT_RATE_VERTEX,
	};

	VkVertexInputAttributeDescription attribute_descriptions[2];

	attribute_descriptions[0].binding = 0;
	attribute_descriptions[0].location = 0;
	attribute_descriptions[0].format = VK_FORMAT_R32G32B32A32_SFLOAT;
	attribute_descriptions[0].offset = 0;

	attribute_descriptions[1].binding = 0;
	attribute_descriptions[1].location = 1;
	attribute_descriptions[1].format = VK_FORMAT_R32G32B32A32_SFLOAT;
	attribute_descriptions[1].offset = sizeof(Vector4);


	VkPipelineVertexInputStateCreateInfo vertexInputInfo = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
		.vertexBindingDescriptionCount = 1,
		.vertexAttributeDescriptionCount = 2,
		.pVertexBindingDescriptions = &binding_description,
		.pVertexAttributeDescriptions = attribute_descriptions,
	};

	VkPipelineInputAssemblyStateCreateInfo input_assembly = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
		.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
		.primitiveRestartEnable = VK_FALSE,
	};

	VkPipelineDepthStencilStateCreateInfo depthStencil = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
		.depthTestEnable = VK_TRUE,
		.depthWriteEnable = VK_TRUE,
		.depthCompareOp = VK_COMPARE_OP_LESS,
		.stencilTestEnable = VK_FALSE,
	};

	VkViewport viewport = {
		.x = 0.0f,
		.y = 0.0f,
		.width = (float)(_swap_chain_image_extent->width),
		.height = (float)(_swap_chain_image_extent->height),
		.minDepth = 0.0f,
		.maxDepth = 1.0f,
	};

	VkOffset2D offset = {
		.x = 0,
		.y = 0,
	};

	VkRect2D scissor = {
		.offset = offset,
		.extent = *_swap_chain_image_extent,
	};

	VkPipelineViewportStateCreateInfo viewportState = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
		.viewportCount = 1,
		.pViewports = &viewport,
		.scissorCount = 1,
		.pScissors = &scissor,
	};

	VkPipelineRasterizationStateCreateInfo rasterizer = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
		.depthClampEnable = VK_FALSE, // VK_TRUE for shadow maps requires enabling GPU feature
		.rasterizerDiscardEnable = VK_FALSE,
		.polygonMode = VK_POLYGON_MODE_FILL,
		.cullMode = VK_CULL_MODE_BACK_BIT,
		.frontFace = VK_FRONT_FACE_CLOCKWISE,
		.lineWidth = 1.0f,
		.depthBiasEnable = VK_FALSE,
		.depthBiasConstantFactor = 0.0f, // Optional
		.depthBiasClamp = 0.0f, // Optional
		.depthBiasSlopeFactor = 0.0f, // Optional
	};

	VkPipelineMultisampleStateCreateInfo multisampling = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
		.sampleShadingEnable = VK_FALSE,
		.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT,
		.minSampleShading = 1.0f, // Optional
		.pSampleMask = NULL, // Optional
		.alphaToCoverageEnable = VK_FALSE, // Optional
		.alphaToOneEnable = VK_FALSE, // Optional
	};

	VkPipelineColorBlendAttachmentState colorBlendAttachment = {
		.colorWriteMask = (
			VK_COLOR_COMPONENT_R_BIT |
			VK_COLOR_COMPONENT_G_BIT |
			VK_COLOR_COMPONENT_B_BIT |
			VK_COLOR_COMPONENT_A_BIT
		),
		.blendEnable = VK_FALSE,
		.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA,
		.dstColorBlendFactor = VK_BLEND_FACTOR_DST_ALPHA,
		.colorBlendOp = VK_BLEND_OP_ADD,
		.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE,
		.dstAlphaBlendFactor = VK_BLEND_FACTOR_ONE,
		.alphaBlendOp = VK_BLEND_OP_ADD,
	};

	VkPipelineColorBlendStateCreateInfo colorBlending = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
		.logicOpEnable = VK_FALSE,
		.logicOp = VK_LOGIC_OP_COPY, // Optional
		.attachmentCount = 1,
		.pAttachments = &colorBlendAttachment,
		.blendConstants[0] = 0.0f, // Optional
		.blendConstants[1] = 0.0f, // Optional
		.blendConstants[2] = 0.0f, // Optional
		.blendConstants[3] = 0.0f, // Optional
	};

	VkDynamicState dynamicStates[] = {
		VK_DYNAMIC_STATE_VIEWPORT,
		VK_DYNAMIC_STATE_LINE_WIDTH
	};

	VkPipelineDynamicStateCreateInfo dynamicState = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
		.dynamicStateCount = 2,
		.pDynamicStates = dynamicStates,
	};

	VkPipelineLayoutCreateInfo pipelineLayoutInfo = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
		.setLayoutCount = 1,
		.pSetLayouts = &_descriptor_set_layout,
		.pushConstantRangeCount = 0, // Optional
		.pPushConstantRanges = 0, // Optional
	};

	PROCESS_VK_RESULT(vkCreatePipelineLayout(_device, &pipelineLayoutInfo, NULL, &_graphics_pipeline_layout));

	VkGraphicsPipelineCreateInfo pipelineInfo = {
		.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
		.stageCount = 2,
		.pStages = shaderStages,
		.pVertexInputState = &vertexInputInfo,
		.pInputAssemblyState = &input_assembly,
		.pViewportState = &viewportState,
		.pRasterizationState = &rasterizer,
		.pMultisampleState = &multisampling,
		.pColorBlendState = &colorBlending,
		.layout = _graphics_pipeline_layout,
		.renderPass = *_render_pass,
		.subpass = 0,
		.basePipelineHandle = VK_NULL_HANDLE,
		.pDepthStencilState = &depthStencil,
	};

	return vkCreateGraphicsPipelines(_device, VK_NULL_HANDLE, 1, &pipelineInfo, NULL, &_graphics_pipeline);
}

static bool _host_visible_to_device_local_buffer_copy(VkBuffer *dst_buffer, VkBuffer *src_buffer, VkDeviceSize size)
{
	VkCommandBufferBeginInfo beginInfo = {
		.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
		.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
	};
	PROCESS_VK_RESULT(vkBeginCommandBuffer(_command_buffers.data[_memory_copy_command_buffer_idx], &beginInfo));

	VkBufferCopy copy_attrs = { .size = size };

	vkCmdCopyBuffer(
		_command_buffers.data[_memory_copy_command_buffer_idx],
		*src_buffer,
		*dst_buffer,
		1, &copy_attrs
	);

	vkEndCommandBuffer(
		_command_buffers.data[_memory_copy_command_buffer_idx]
	);

	VkSubmitInfo submit_info = {
		.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
		.commandBufferCount = 1,
		.pCommandBuffers = _command_buffers.data + _memory_copy_command_buffer_idx,
	};
	PROCESS_VK_RESULT(vkQueueSubmit(_present_queue, 1, &submit_info, VK_NULL_HANDLE));
	PROCESS_VK_RESULT(vkQueueWaitIdle(_present_queue));

	return true;
}
static bool _physical_device_supports_required_extensions(const VkPhysicalDevice* physical_device) {
	uint32_t supported_exts_num;
	vkEnumerateDeviceExtensionProperties(*physical_device, NULL, &supported_exts_num, NULL);

	VkExtensionProperties *supported_extensions = (VkExtensionProperties *)malloc(sizeof(VkExtensionProperties) * supported_exts_num);

	vkEnumerateDeviceExtensionProperties(*physical_device, NULL, &supported_exts_num, supported_extensions);

	uint32_t matches = 0;

	for (uint32_t i = 0; i < _required_physical_device_extensions.count; i += 1)
	{
		for (uint32_t j = 0; j < supported_exts_num; j += 1)
		{
			VkExtensionProperties *supported_extension = supported_extensions + j;

			if (strcmp(supported_extension->extensionName, _required_physical_device_extensions.names[i]) == 0)
			{
				matches += 1;
			}
		}
	}

	bool result = (matches == _required_physical_device_extensions.count);

	free(supported_extensions);

	return result;
}
static bool _set_queue_family_properties(const VkPhysicalDevice *physical_device)
{
	uint32_t queue_families_num = 0;
	vkGetPhysicalDeviceQueueFamilyProperties(*physical_device, &queue_families_num, NULL);

	VkQueueFamilyProperties *queue_families = malloc(sizeof(VkQueueFamilyProperties) * queue_families_num);
	vkGetPhysicalDeviceQueueFamilyProperties(*physical_device, &queue_families_num, queue_families);

	bool result = false;

	for (uint32_t i = 0; i < queue_families_num; i += 1)
	{
		if (
			queue_families[i].queueCount > 0 &&
			queue_families[i].queueFlags & VK_QUEUE_GRAPHICS_BIT
		) {
			_operation_queue_families.graphics_family_idx = (int)i;
		}

		if (
			queue_families[i].queueCount > 0 &&
			queue_families[i].queueFlags & VK_QUEUE_COMPUTE_BIT
		) {
			_operation_queue_families.compute_family_idx = (int)i;
		}

		VkBool32 present_support;

		vkGetPhysicalDeviceSurfaceSupportKHR(*physical_device, i, _surface, &present_support);

		if (queue_families[i].queueCount > 0 && present_support)
		{
			_operation_queue_families.present_family_idx = (int)i;
		}

		if (
			_operation_queue_families.graphics_family_idx >= 0 &&
			_operation_queue_families.present_family_idx >= 0 &&
			_operation_queue_families.compute_family_idx >= 0
		) {
			result = true;

			_operation_queue_families.use_same_family = (
				_operation_queue_families.graphics_family_idx == _operation_queue_families.present_family_idx &&
				_operation_queue_families.present_family_idx == _operation_queue_families.compute_family_idx
			);

			break;
		}
	}

	free(queue_families);

	return result;
}
static bool _physical_device_suitable(const VkPhysicalDevice* physical_device)
{
	VkPhysicalDeviceProperties device_properties;
	vkGetPhysicalDeviceProperties(*physical_device, &device_properties);

	if (!_set_queue_family_properties(physical_device))
	{
		return false;
	}

	bool extensions_supported = _physical_device_supports_required_extensions(physical_device);

	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(*physical_device, _surface, &_surface_capabilities);
	vkGetPhysicalDeviceMemoryProperties(*physical_device, &_supported_memory_properties);
	vkGetPhysicalDeviceSurfaceFormatsKHR(*physical_device, _surface, &(_supported_surface_formats.count), NULL);

	if (_supported_surface_formats.count != 0) {
		_supported_surface_formats.data = (VkSurfaceFormatKHR*)malloc(sizeof(VkSurfaceFormatKHR) * _supported_surface_formats.count);

		vkGetPhysicalDeviceSurfaceFormatsKHR(*physical_device, _surface, &(_supported_surface_formats.count), _supported_surface_formats.data);
	}

	vkGetPhysicalDeviceSurfacePresentModesKHR(*physical_device, _surface, &(_supported_present_modes.count), NULL);

	if (_supported_present_modes.count != 0) {
		_supported_present_modes.data = (VkPresentModeKHR*)malloc(sizeof(VkPresentModeKHR) * _supported_present_modes.count);

		vkGetPhysicalDeviceSurfacePresentModesKHR(*physical_device, _surface, &(_supported_present_modes.count), _supported_present_modes.data);
	}

	bool surface_output_supported = (
		_supported_surface_formats.count != 0 &&
		extensions_supported &&
		_supported_present_modes.count != 0
	);

	return (
		device_properties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU &&
		_operation_queue_families.use_same_family &&
		surface_output_supported
	);
}
static bool _create_memory_buffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, VkBuffer *buffer, VkDeviceMemory *buffer_memory)
{
	VkBufferCreateInfo bufferInfo = {
		.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
		.size = size,
		.usage = usage,
		.sharingMode = VK_SHARING_MODE_EXCLUSIVE,
	};

	PROCESS_VK_RESULT(vkCreateBuffer(_device, &bufferInfo, NULL, buffer));

	VkMemoryRequirements memRequirements;
	vkGetBufferMemoryRequirements(_device, *buffer, &memRequirements);

	VkMemoryAllocateInfo allocInfo = {
		.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
		.allocationSize = memRequirements.size,
	};

	VkPhysicalDeviceMemoryProperties memProperties;
	vkGetPhysicalDeviceMemoryProperties(_physical_device, &memProperties);

	bool suitable_memory_found = false;

	for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++) {
		if (
			memRequirements.memoryTypeBits & (1 << i) &&
			(memProperties.memoryTypes[i].propertyFlags & properties) == properties
		) {
			allocInfo.memoryTypeIndex = i;

			suitable_memory_found = true;
		}
	}

	PROCESS_RESULT(suitable_memory_found, "Couldn't find suitable memory type");
	PROCESS_VK_RESULT(vkAllocateMemory(_device, &allocInfo, NULL, buffer_memory));
	PROCESS_VK_RESULT(vkBindBufferMemory(_device, *buffer, *buffer_memory, 0));

	return true;
}
static bool _pick_physical_device()
{
	uint32_t devices_num = 0;

	vkEnumeratePhysicalDevices(_instance, &devices_num, NULL);

	VkPhysicalDevice *devices = malloc(sizeof(VkPhysicalDevice) * devices_num);
	vkEnumeratePhysicalDevices(_instance, &devices_num, devices);

	bool result = true;

	_required_physical_device_extensions.names = (const char**)malloc(sizeof(const char*) * PHYSICAL_DEVICE_EXTENSIONS_COUNT);
	_required_physical_device_extensions.names[0] = VK_KHR_SWAPCHAIN_EXTENSION_NAME;
	_required_physical_device_extensions.count = PHYSICAL_DEVICE_EXTENSIONS_COUNT;

	if (devices_num == 0)
	{
		result = false;
	}
	else
	{
		for (uint32_t i = 0; i < devices_num; i += 1) {
			if (_physical_device_suitable(devices + i)) {
				_physical_device = devices[i];

				break;
			}
		}

		if (_physical_device == VK_NULL_HANDLE) {
			result = false;
		}
	}

	free(devices);

	return result;
};
static bool _create_descriptor_sets()
{
	VkDescriptorSetAllocateInfo alloc_info = {
		.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
		.pNext = NULL,
		.descriptorPool = _descriptor_pool,
		.pSetLayouts = &_descriptor_set_layout,
		.descriptorSetCount = 1,
	};

	PROCESS_VK_RESULT(vkAllocateDescriptorSets(_device, &alloc_info, &_descriptor_set));

	VkDescriptorBufferInfo vertex_buffer_info = {
		.buffer = _device_vertex_buffer,
		.offset = 0,
		.range = VK_WHOLE_SIZE,
	};

	VkDescriptorBufferInfo index_buffer_info = {
		.buffer = _device_index_buffer,
		.offset = 0,
		.range = VK_WHOLE_SIZE,
	};

	VkDescriptorBufferInfo mvp_buffer_info = {
		.buffer = _device_uniform_data_buffer,
		.offset = 0,
		.range = VK_WHOLE_SIZE,
	};

	VkDescriptorBufferInfo particle_buffer_info = {
		.buffer = _device_particle_buffer,
		.offset = 0,
		.range = VK_WHOLE_SIZE,
	};

	VkWriteDescriptorSet vertex_write_descriptor_set = {
		.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
		.dstSet = _descriptor_set,
		.dstBinding = 0,
		.dstArrayElement = 0,
		.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
		.descriptorCount = 1,
		.pBufferInfo = &vertex_buffer_info,
	};

	VkWriteDescriptorSet index_write_descriptor_set = {
		.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
		.dstSet = _descriptor_set,
		.dstBinding = 1,
		.dstArrayElement = 0,
		.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
		.descriptorCount = 1,
		.pBufferInfo = &index_buffer_info,
	};

	VkWriteDescriptorSet uniform_data_write_descriptor_set = {
		.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
		.dstSet = _descriptor_set,
		.dstBinding = 2,
		.dstArrayElement = 0,
		.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
		.descriptorCount = 1,
		.pBufferInfo = &mvp_buffer_info,
	};

	VkWriteDescriptorSet particle_write_descriptor_set = {
		.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
		.dstSet = _descriptor_set,
		.dstBinding = 3,
		.dstArrayElement = 0,
		.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
		.descriptorCount = 1,
		.pBufferInfo = &particle_buffer_info,
	};

	VkWriteDescriptorSet write_descriptor_sets[GPU_DATA_BINDINGS_COUNT] = {
		vertex_write_descriptor_set,
		index_write_descriptor_set,
		uniform_data_write_descriptor_set,
		particle_write_descriptor_set
	};

	vkUpdateDescriptorSets(_device, GPU_DATA_BINDINGS_COUNT, write_descriptor_sets, 0, NULL);

	return true;
}
static bool _create_uniform_data_buffer()
{
	VkDeviceSize buffer_size = sizeof(UniformData);

	PROCESS_RESULT(
		_create_memory_buffer(
			buffer_size,
			VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
			&_host_uniform_data_buffer,
			&_host_uniform_data_buffer_memory
		),
		"Failed to create host uniform data memory buffer"
	);

	PROCESS_RESULT(
		_create_memory_buffer(
			buffer_size,
			VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
			VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
			&_device_uniform_data_buffer,
			&_device_uniform_data_buffer_memory
		),
		"Failed to create device uniform data memory buffer"
	);

	return true;
}
static bool _create_vertex_buffer()
{
	VkDeviceSize buffer_size = (VkDeviceSize)sizeof(Vertex) * MAX_PARTICLES_COUNT * 4;

	PROCESS_RESULT(
		_create_memory_buffer(
			buffer_size,
			VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
			VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
			&_device_vertex_buffer,
			&_device_vertex_buffer_memory
		),
		"Failed to create device vertex memory buffer"
	);

	return true;
}
static bool _create_index_buffer()
{
	VkDeviceSize buffer_size = (VkDeviceSize)sizeof(uint32_t) * MAX_PARTICLES_COUNT * 6;

	PROCESS_RESULT(
		_create_memory_buffer(
			buffer_size,
			VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
			VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
			&_device_index_buffer,
			&_device_index_buffer_memory
		),
		"Failed to create device index memory buffer"
	);

	return true;
}
static bool _create_particle_buffer()
{
	VkDeviceSize buffer_size = (VkDeviceSize)sizeof(Particle) * MAX_PARTICLES_COUNT;
	VkDeviceSize initially_loaded_size = (VkDeviceSize)sizeof(Particle) * INITIAL_PARTICLES_COUNT;

	PROCESS_RESULT(
		_create_memory_buffer(
			buffer_size,
			VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
			&_host_particle_buffer,
			&_host_particle_buffer_memory
		),
		"Failed to create host particle memory buffer"
	);

	void *data;

	vkMapMemory(_device, _host_particle_buffer_memory, 0, buffer_size, 0, &data);
	memcpy(data, _particles.data, (size_t)initially_loaded_size);
	vkUnmapMemory(_device, _host_particle_buffer_memory);

	PROCESS_RESULT(
		_create_memory_buffer(
			buffer_size,
			VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
			VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
			&_device_particle_buffer,
			&_device_particle_buffer_memory
		),
		"Failed to create device particle memory buffer"
	);

	return _host_visible_to_device_local_buffer_copy(&_device_particle_buffer, &_host_particle_buffer, initially_loaded_size);
}
static bool _create_command_buffers()
{
	const SwapChainImages* _swap_chain_images = swap_chain_get_images();

	VkCommandPoolCreateInfo command_buffer_pool_ci = {
		.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
		.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
		.queueFamilyIndex = _operation_queue_families.graphics_family_idx,  // _operation_queue_families.use_same_family == true
	};

	_memory_copy_command_buffer_idx = 0;
	_image_draw_command_buffers_begin_idx = 1;
	_compute_command_buffer_idx = _image_draw_command_buffers_begin_idx + _swap_chain_images->count;

	_command_buffers.count = 2 + _swap_chain_images->count;
	_command_buffers.data = (VkCommandBuffer*)malloc(sizeof(VkCommandBuffer) * _command_buffers.count);

	PROCESS_VK_RESULT(vkCreateCommandPool(_device, &command_buffer_pool_ci, NULL, &_command_buffer_pool));

	VkCommandBufferAllocateInfo memory_copy_command_buffer_ai = {
		.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
		.commandPool = _command_buffer_pool,
		.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
		.commandBufferCount = 1,
	};
	PROCESS_VK_RESULT(vkAllocateCommandBuffers(_device, &memory_copy_command_buffer_ai, _command_buffers.data + _memory_copy_command_buffer_idx));

	VkCommandBufferAllocateInfo image_draw_cb_ai = {
		.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
		.commandPool = _command_buffer_pool,
		.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
		.commandBufferCount = _swap_chain_images->count + 1,
	};
	PROCESS_VK_RESULT(vkAllocateCommandBuffers(_device, &image_draw_cb_ai, _command_buffers.data + _image_draw_command_buffers_begin_idx));

	VkCommandBufferAllocateInfo compute_cb_ai = {
		.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
		.commandPool = _command_buffer_pool,
		.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
		.commandBufferCount = 1,
	};
	PROCESS_VK_RESULT(vkAllocateCommandBuffers(_device, &compute_cb_ai, _command_buffers.data + _compute_command_buffer_idx));

	return true;
}
static bool _write_image_draw_command_buffers()
{
	const VkExtent2D* _swap_chain_image_extent = swap_chain_get_image_extent();
	const VkRenderPass* _render_pass = swap_chain_get_render_pass();
	const SwapChainImages* _swap_chain_images = swap_chain_get_images();
	const SwapChainFramebuffers* _swap_chain_framebuffers = swap_chain_get_framebuffers();

	for (uint32_t i = 0; i < _swap_chain_images->count; i++)
	{
		uint32_t idx = _image_draw_command_buffers_begin_idx + i;

		VkCommandBufferBeginInfo beginInfo = {
			.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
			.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT,
			.pInheritanceInfo = NULL, // Optional
		};
		vkBeginCommandBuffer(_command_buffers.data[idx], &beginInfo);

		VkRenderPassBeginInfo renderPassInfo = {
			.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
			.renderPass = *_render_pass,
			.framebuffer = _swap_chain_framebuffers->data[i],
			.renderArea.offset = { 0, 0 },
			.renderArea.extent = *_swap_chain_image_extent,
		};

		VkClearValue clearValues[2];
		VkClearColorValue clear_color = {
			.float32 = { 0.0f, 0.0f, 0.0f, 0.0f },
		};
		VkClearDepthStencilValue clear_depth_stencil = {
			.depth = 1.0f,
			.stencil = 0,
		};
		clearValues[0].color = clear_color;
		clearValues[1].depthStencil = clear_depth_stencil;

		renderPassInfo.clearValueCount = 2;
		renderPassInfo.pClearValues = clearValues;

		vkCmdBeginRenderPass(_command_buffers.data[idx], &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

		vkCmdBindPipeline(_command_buffers.data[idx], VK_PIPELINE_BIND_POINT_GRAPHICS, _graphics_pipeline);

		VkDeviceSize offsets[] = { 0 };

		vkCmdBindDescriptorSets(
			_command_buffers.data[idx],
			VK_PIPELINE_BIND_POINT_GRAPHICS,
			_graphics_pipeline_layout, 0, 1,
			&_descriptor_set, 0, NULL
		);

		vkCmdBindVertexBuffers(_command_buffers.data[idx], 0, 1, &_device_vertex_buffer, offsets);
		vkCmdBindIndexBuffer(_command_buffers.data[idx], _device_index_buffer, 0, VK_INDEX_TYPE_UINT32);
		vkCmdDrawIndexed(_command_buffers.data[idx], _indices.count, 1, 0, 0, 0);
		vkCmdEndRenderPass(_command_buffers.data[idx]);

		PROCESS_VK_RESULT(vkEndCommandBuffer(_command_buffers.data[idx]));
	}

	return true;
}
static bool _write_compute_command_buffer()
{
	VkCommandBufferBeginInfo beginInfo = {
		.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
	};

	PROCESS_VK_RESULT(
		vkBeginCommandBuffer(
			_command_buffers.data[_compute_command_buffer_idx],
			&beginInfo
		)
	);

	/*
		Add memory barrier to ensure that the (graphics) vertex shader
		has fetched attributes before compute starts to write to the buffer.
	*/
	VkBufferMemoryBarrier bufferMemoryBarrier = {
		.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER,
		.buffer = _device_particle_buffer,
		.size = VK_WHOLE_SIZE,
		//.srcAccessMask = VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT,  // Vertex shader invocations have finished reading from the buffer
		//.dstAccessMask = VK_ACCESS_SHADER_WRITE_BIT,           // Compute shader wants to write to the buffer
		/*
			Compute and graphics queue may have different queue families.
			For the barrier to work across different queues,
			we need to set their family indices.
		*/
		.srcQueueFamilyIndex = _operation_queue_families.graphics_family_idx,  // Required as compute and graphics queue may have different families
		.dstQueueFamilyIndex = _operation_queue_families.compute_family_idx,    // Required as compute and graphics queue may have different families
	};

	vkCmdPipelineBarrier(
		_command_buffers.data[_compute_command_buffer_idx],
		VK_PIPELINE_STAGE_VERTEX_SHADER_BIT,
		VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT,
		0,
		0, NULL,
		1, &bufferMemoryBarrier,
		0, NULL
	);

	vkCmdBindPipeline(
		_command_buffers.data[_compute_command_buffer_idx],
		VK_PIPELINE_BIND_POINT_COMPUTE,
		_compute_pipeline
	);
	vkCmdBindDescriptorSets(
		_command_buffers.data[_compute_command_buffer_idx],
		VK_PIPELINE_BIND_POINT_COMPUTE,
		_compute_pipeline_layout, 0, 1, &_descriptor_set, 0, 0
	);

	// Dispatch the compute job
	vkCmdDispatch(_command_buffers.data[_compute_command_buffer_idx], _particles.count, 1, 1);

	// Add memory barrier to ensure that compute shader has finished writing to the buffer
	// Without this the (rendering) vertex shader may display incomplete results (partial data from last frame) 
	//bufferMemoryBarrier.srcAccessMask = VK_ACCESS_SHADER_WRITE_BIT;          // Compute shader has finished writes to the buffer
	//bufferMemoryBarrier.dstAccessMask = VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT; // Vertex shader invocations want to read from the buffer
	bufferMemoryBarrier.buffer = _device_particle_buffer;
	bufferMemoryBarrier.size = VK_WHOLE_SIZE;
	bufferMemoryBarrier.srcQueueFamilyIndex = _operation_queue_families.compute_family_idx;
	bufferMemoryBarrier.dstQueueFamilyIndex = _operation_queue_families.graphics_family_idx;

	vkCmdPipelineBarrier(
		_command_buffers.data[_compute_command_buffer_idx],
		VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT,
		VK_PIPELINE_STAGE_VERTEX_SHADER_BIT,
		0,
		0, NULL, 1, &bufferMemoryBarrier, 0, NULL
	);

	return vkEndCommandBuffer(_command_buffers.data[_compute_command_buffer_idx]) == VK_SUCCESS;
}

static bool _draw_frame()
{
	const VkSwapchainKHR* _swap_chain = swap_chain_get();

	vkResetFences(_device, 1, &_compute_finished_fence);

	uint32_t image_index;

	VkResult acquire_image_result = vkAcquireNextImageKHR(
		_device,
		*_swap_chain,
		1000,
		_image_available,
		VK_NULL_HANDLE,
		&image_index
	);

	VkSubmitInfo compute_queue_submit_info = {
	.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
	.pNext = NULL,
	.commandBufferCount = 1,
	.pCommandBuffers = _command_buffers.data + _compute_command_buffer_idx,
	};

	PROCESS_VK_RESULT(vkQueueSubmit(_compute_queue, 1, &compute_queue_submit_info, _compute_finished_fence));

	VkFlags wait_dst_stage_flags = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;

	VkSubmitInfo graphics_queue_submit_info = {
		.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
		.waitSemaphoreCount = 1,
		.pWaitSemaphores = &_image_available,
		.pWaitDstStageMask = &wait_dst_stage_flags,
		.commandBufferCount = 1,
		.pCommandBuffers = (
			_command_buffers.data + _image_draw_command_buffers_begin_idx + image_index
		),
		.signalSemaphoreCount = 1,
		.pSignalSemaphores = &_render_finished,
	};

	vkWaitForFences(_device, 1, &_compute_finished_fence, VK_TRUE, UINT64_MAX);

	PROCESS_VK_RESULT(vkQueueSubmit(_graphics_queue, 1, &graphics_queue_submit_info, VK_NULL_HANDLE));

	VkPresentInfoKHR presentInfo = {
		.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
		.waitSemaphoreCount = 1,
		.pWaitSemaphores = &_render_finished,
		.swapchainCount = 1,
		.pSwapchains = _swap_chain,
		.pImageIndices = &image_index,
	};

	PROCESS_VK_RESULT(vkQueuePresentKHR(_present_queue, &presentInfo));

	return true;
}
static bool _update_uniform_data_buffer(UniformData* uniform_data)
{
	VkDeviceSize buffer_size = sizeof(UniformData);

	void* data;

	vkMapMemory(_device, _host_uniform_data_buffer_memory, 0, buffer_size, 0, &data);
	memcpy(data, uniform_data, (uint32_t)buffer_size);
	vkUnmapMemory(_device, _host_uniform_data_buffer_memory);

	return _host_visible_to_device_local_buffer_copy(&_device_uniform_data_buffer, &_host_uniform_data_buffer, buffer_size);
}

static void _cursor_position_callback(GLFWwindow* window, double position_x, double position_y)
{
	application_track_mouse_position((float)position_x, (float)position_y);
}
static void _mouse_button_callback(GLFWwindow *window, int mouse_button, int action, int mods)
{
	application_select_area(mouse_button, action);
}
static void _keyboard_key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	application_toggle_screen_rotation(key, action);
}
static void _glfw_error_callback(int glfw_errno, const char* error_description)
{
	printf("%s\n", error_description);
}
static void _window_resize_callback(GLFWwindow* window, int width, int height)
{
	vkDeviceWaitIdle(_device);

	swap_chain_destroy();
	vkDestroySurfaceKHR(_instance, _surface, NULL);
	glfwCreateWindowSurface(_instance, window, NULL, &(_surface));
	swap_chain_create();

	_write_image_draw_command_buffers();
}

bool setup_window_and_gpu()
{
	PROCESS_RESULT(glfwInit(), "Failed to initialize GLFW");
	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API); // don't create OpenGL context
	glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE); // enable resizing

	_window = glfwCreateWindow(DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT, "zGame", NULL, NULL);

	glfwSetMouseButtonCallback(_window, _mouse_button_callback);
	glfwSetCursorPosCallback(_window, _cursor_position_callback);
	glfwSetKeyCallback(_window, _keyboard_key_callback);
	glfwSetErrorCallback(_glfw_error_callback);
	glfwSetWindowSizeCallback(_window, _window_resize_callback);

	uint32_t required_instance_extensions_count;
	char** required_instance_extensions = glfwGetRequiredInstanceExtensions(&required_instance_extensions_count);

	PROCESS_RESULT(_create_instance(required_instance_extensions_count, required_instance_extensions), "Failed to create VkInstance");

	PROCESS_VK_RESULT(glfwCreateWindowSurface(_instance, _window, NULL, &(_surface)));
	PROCESS_RESULT(_pick_physical_device(), "Failed to pick physical device");
	PROCESS_VK_RESULT(_create_logical_device());

	vkGetDeviceQueue(_device, _operation_queue_families.compute_family_idx, 0, &_compute_queue);
	vkGetDeviceQueue(_device, _operation_queue_families.graphics_family_idx, 1, &_graphics_queue);
	vkGetDeviceQueue(_device, _operation_queue_families.present_family_idx, 2, &_present_queue);

	PROCESS_RESULT(swap_chain_create(), "Failed to create swap chain");
	PROCESS_RESULT(_create_command_buffers(), "Failed to create commands");

	PROCESS_RESULT(_create_vertex_buffer(), "Failed to create vertex buffer");
	PROCESS_RESULT(_create_index_buffer(), "Failed to create index buffer");
	PROCESS_RESULT(_create_particle_buffer(), "Failed to create particle buffer");
	PROCESS_RESULT(_create_uniform_data_buffer(), "Failed to create uniform data buffer");
	PROCESS_VK_RESULT(_create_descriptor_pool());
	PROCESS_VK_RESULT(_create_descriptor_set_layout());
	PROCESS_RESULT(_create_descriptor_sets(), "Failed to create descriptor sets");
	PROCESS_VK_RESULT(_create_compute_pipeline());

	VkFenceCreateInfo fence_ci = {
		.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
		.flags = VK_FENCE_CREATE_SIGNALED_BIT,
	};

	PROCESS_VK_RESULT(vkCreateFence(_device, &fence_ci, NULL, &_compute_finished_fence));
	PROCESS_VK_RESULT(_create_graphics_pipeline());

	VkSemaphoreCreateInfo semaphore_ci = {
		.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO
	};

	PROCESS_VK_RESULT(vkCreateSemaphore(_device, &semaphore_ci, NULL, &_image_available));
	PROCESS_VK_RESULT(vkCreateSemaphore(_device, &semaphore_ci, NULL, &_render_finished));

	PROCESS_RESULT(_write_image_draw_command_buffers(), "Failed to write image draw command buffers");
	PROCESS_RESULT(_write_compute_command_buffer(), "Failed to write compute command buffer");

	return true;
}
void create_particles()
{
	_particles.count = INITIAL_PARTICLES_COUNT;
	_particles.data = (Particle*)malloc(sizeof(Particle) * _particles.count);

	Particle p0 = {
		.position = { 0.5f, 0.5f, 0.0f, 1.0f },
		.color = { 1.0f, 0.0f, 0.0f, 1.0f },
		.radius = 0.01f,
	};
	_particles.data[0] = p0;

	Particle p1 = {
		.position = { 0.0f, 0.0f, 0.0f, 1.0f },
		.color = { 0.0f, 1.0f, 0.0f, 1.0f },
		.radius = 0.02f,
	};
	_particles.data[1] = p1;

	Particle p2 = {
		.position = { 0.5f, 0.0f, 0.0f, 1.0f },
		.color = { 0.0f, 0.0f, 1.0f, 1.0f },
		.radius = 0.03f,
	};
	_particles.data[2] = p2;

	Particle p3 = {
		.position = { 0.0f, 0.5f, 0.0f, 1.0f },
		.color = { 0.5f, 0.5f, 0.5f, 1.0f },
		.radius = 0.04f,
	};
	_particles.data[3] = p3;

	Particle p4 = {
		.position = { 0.5f, 0.5f, 0.5f, 1.0f },
		.color = { 0.5f, 0.5f, 0.0f, 1.0f },
		.radius = 0.05f,
	};
	_particles.data[4] = p4;

	Particle p5 = {
		.position = { 0.0f, 0.0f, 0.5f, 1.0f },
		.color = { 0.0f, 0.5f, 0.5f, 1.0f },
		.radius = 0.06f,
	};
	_particles.data[5] = p5;

	Particle p6 = {
		.position = { 0.5f, 0.0f, 0.5f, 1.0f },
		.color = { 0.5f, 0.0f, 0.5f, 1.0f },
		.radius = 0.07f,
	};
	_particles.data[6] = p6;

	Particle p7 = {
		.position = { 0.0f, 0.5f, 0.5f, 1.0f },
		.color = { 1.0f, 1.0f, 1.0f, 1.0f },
		.radius = 0.08f,
	};
	_particles.data[7] = p7;

	_vertices.count = _particles.count * 4;
	_vertices.data = (Vertex*)malloc(sizeof(Vertex) * _vertices.count);

	_indices.count = _particles.count * 6;
	_indices.data = (uint32_t*)malloc(sizeof(uint32_t) * _indices.count);
}
void destroy_particles()
{
	free(_particles.data);
	free(_vertices.data);
	free(_indices.data);
}
bool render()
{
	clock_t t0, t1;
	float time_diff = 0.0f;

	application_init_scene();

	const VkExtent2D* _swap_chain_image_extent = swap_chain_get_image_extent();

	while (!glfwWindowShouldClose(_window))
	{
		glfwPollEvents();

		t0 = clock();

		UniformData* uniform_data = application_render_scene(
			_swap_chain_image_extent->width,
			_swap_chain_image_extent->height
		);
		application_rotate_scene(time_diff);

		PROCESS_RESULT(_update_uniform_data_buffer(uniform_data), "Failed to update uniform data buffer");
		PROCESS_RESULT(_draw_frame(), "Failed to draw frame");

		t1 = clock();

		time_diff = (float)(t1 - t0) / CLOCKS_PER_SEC;
	}

	vkDeviceWaitIdle(_device);

	return true;
}
void add_particle(float x, float y, float z)
{
	Particle p = {
		.position = { 0.7f, 0.7f, 0.7f, 1.0f },
		.color = { 0.5f, 0.5f, 0.0f, 1.0f },
		.radius = 0.05f,
	};
	_particles.count += 1;
	_vertices.count += 4;
	_indices.count += 6;

	VkDeviceSize new_data_size = sizeof(Particle) * _particles.count;
	Particle* new_particles_data = (Particle*)malloc((size_t)new_data_size);

	memcpy(new_particles_data, _particles.data, sizeof(Particle) * _particles.count);
	memcpy(&new_particles_data[_particles.count - 1], &p, sizeof(Particle));

	free(_particles.data);
	_particles.data = new_particles_data;

	vkDeviceWaitIdle(_device);

	void* data;

	vkMapMemory(_device, _host_particle_buffer_memory, 0, new_data_size, 0, &data);
	memcpy(data, _particles.data, (size_t)new_data_size);
	vkUnmapMemory(_device, _host_particle_buffer_memory);

	_host_visible_to_device_local_buffer_copy(&_device_particle_buffer, &_host_particle_buffer, new_data_size);

	_write_compute_command_buffer();
	_write_image_draw_command_buffers();
}
void _destroy_memory_buffers()
{
	vkDestroyBuffer(_device, _host_particle_buffer, NULL);
	vkFreeMemory(_device, _host_particle_buffer_memory, NULL);
	vkDestroyBuffer(_device, _device_particle_buffer, NULL);
	vkFreeMemory(_device, _device_particle_buffer_memory, NULL);
	vkDestroyBuffer(_device, _device_vertex_buffer, NULL);
	vkFreeMemory(_device, _device_vertex_buffer_memory, NULL);
	vkDestroyBuffer(_device, _host_index_buffer, NULL);
	vkFreeMemory(_device, _host_index_buffer_memory, NULL);
	vkDestroyBuffer(_device, _device_index_buffer, NULL);
	vkFreeMemory(_device, _device_index_buffer_memory, NULL);
	vkDestroyBuffer(_device, _host_uniform_data_buffer, NULL);
	vkFreeMemory(_device, _host_uniform_data_buffer_memory, NULL);
	vkDestroyBuffer(_device, _device_uniform_data_buffer, NULL);
	vkFreeMemory(_device, _device_uniform_data_buffer_memory, NULL);
}
void destroy_window_and_free_gpu()
{
	_destroy_memory_buffers();

	vkDestroyDescriptorSetLayout(_device, _descriptor_set_layout, NULL);
	vkDestroyDescriptorPool(_device, _descriptor_pool, NULL);
	vkDestroyFence(_device, _compute_finished_fence, NULL);
	vkDestroySemaphore(_device, _image_available, NULL);
	vkDestroySemaphore(_device, _render_finished, NULL);
	vkDestroyPipeline(_device, _compute_pipeline, NULL);
	vkDestroyPipelineLayout(_device, _compute_pipeline_layout, NULL);
	vkDestroyShaderModule(_device, _vertex_shader, NULL);
	vkDestroyShaderModule(_device, _fragment_shader, NULL);
	vkDestroyShaderModule(_device, _compute_shader, NULL);

	swap_chain_destroy();

	vkFreeCommandBuffers(_device, _command_buffer_pool, 1, _command_buffers.data + _compute_command_buffer_idx);
	vkDestroyPipeline(_device, _graphics_pipeline, NULL);
	vkDestroyPipelineLayout(_device, _graphics_pipeline_layout, NULL);
	vkDestroyCommandPool(_device, _command_buffer_pool, NULL);
	vkDestroySurfaceKHR(_instance, _surface, NULL);
	vkDestroyDevice(_device, NULL);
	vkDestroyInstance(_instance, NULL);

	free(_supported_surface_formats.data);
	free(_supported_present_modes.data);
	free((void*)_required_physical_device_extensions.names);
	free(_vertex_shader_code);
	free(_fragment_shader_code);
	free(_compute_shader_code);
}

const VkDevice* system_bridge_get_device() {
	return &_device;
}
const SurfaceFormats* system_bridge_get_supported_surface_formats() {
	return &_supported_surface_formats;
}
const VkSurfaceCapabilitiesKHR* system_bridge_get_surface_capabilities() {
	return &_surface_capabilities;
}
const VkSurfaceKHR* system_bridge_get_surface() {
	return &_surface;
}
const GLFWwindow* system_bridge_get_window() {
	return _window;
}
const VkPhysicalDeviceMemoryProperties* system_bridge_get_supported_memory_properties() {
	return &_supported_memory_properties;
}
