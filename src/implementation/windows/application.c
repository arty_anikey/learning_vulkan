#include "application.h"
#include "system_bridge.h"

#include <stdio.h>
#include <stdbool.h>

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#define ACTION_ROTATE 0
uint32_t actions[1] = { GLFW_KEY_E };
bool _is_rotating = false;

static Matrix4x4 _projection = {
	.data = {
		1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	}
};
static Matrix4x4 _view = {
	.data = {
		1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	}
};
static Matrix4x4 _model = {
	.data = {
		1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	}
};

Quaternion _base = { .x = 0.0f, .y = 0.0f, .z = 0.0f, .w = 1.0f };

static UniformData _uniform_data;

static const Vector3 _eye = { 0.0f, 0.0f, -1.5f };
static const Vector3 _up = { 0.0f, -1.0f, -1.0f };
static const Vector3 _look_at = { 0.0f, 0.0f, 0.0f };

static bool _mouse_button_left_pressed = false;
static Vector2 _cursor_current_position;
static Vector2 _mouse_select_area_start;
static Vector2 _mouse_select_area_end;

void application_track_mouse_position(float x, float y)
{
	_cursor_current_position.x = (float)x;
	_cursor_current_position.y = (float)y;
}
void application_select_area(int mouse_button, int glfw_action)
{
	if (mouse_button == GLFW_MOUSE_BUTTON_LEFT)
	{
		if (glfw_action == GLFW_PRESS)
		{
			_mouse_select_area_start = _cursor_current_position;

			_mouse_button_left_pressed = true;
		}
		if (glfw_action == GLFW_RELEASE)
		{
			if (_mouse_button_left_pressed)
			{
				_mouse_select_area_end = _cursor_current_position;
				_mouse_button_left_pressed = false;

				printf(
					"Mouse select area start: [%f, %f]\n",
					_mouse_select_area_start.x,
					_mouse_select_area_start.y
				);
				printf(
					"Mouse select area end: [%f, %f]\n",
					_mouse_select_area_end.x,
					_mouse_select_area_end.y
				);
				add_particle(0, 0, 0);
			}
		}
	}
}
void application_toggle_screen_rotation(int key, int glfw_action)
{
	if (actions[ACTION_ROTATE] == GLFW_KEY_E)
	{
		if (glfw_action == GLFW_PRESS)
		{
			_is_rotating = true;
		}
		if (glfw_action == GLFW_RELEASE)
		{
			_is_rotating = false;
		}
	}
}
void application_init_scene()
{
	_uniform_data.model = _model;
	_uniform_data.projection = _projection;
	_uniform_data.view = _view;
}
UniformData* application_render_scene(uint32_t width, uint32_t height)
{
	update_perspective_projection_matrix(
		&_projection,
		(float)M_PI / 2.0f,
		(float)(width / height),
		0.1f,
		10.0f
	);

	update_view_matrix(&_view, &_eye, &_look_at, &_up);

	_uniform_data.view = _view;
	_uniform_data.projection = _projection;

	return &_uniform_data;
}
void application_rotate_scene(float time_diff)
{
	if (!_is_rotating)
	{
		return;
	}

	Quaternion rotated = get_quaternion(((float)M_PI / 2.0f) * time_diff, &_up);
	_base = get_multiplied_q(&_base, &rotated);

	_uniform_data.model = get_transform(&_base);
}
