#include <stdlib.h>
#include <stdio.h>

#include "system_bridge.h"
#include "swap_chain.h"

static VkSwapchainKHR _swap_chain;
static VkExtent2D _swap_chain_image_extent;
static VkFormat _swap_chain_image_format;
static SwapChainImages _swap_chain_images;
static SwapChainImageViews _swap_chain_image_views;
static SwapChainFramebuffers _swap_chain_framebuffers;
static VkImage _depth_image;
static VkImageView _depth_image_view;
static VkDeviceMemory _depth_image_memory;
static VkFormat _depth_image_format = VK_FORMAT_D32_SFLOAT;
static VkRenderPass _render_pass;


static VkResult _create_render_pass(const VkDevice* _device)
{
	VkAttachmentDescription color_attachment = {
		.format = _swap_chain_image_format,
		.samples = VK_SAMPLE_COUNT_1_BIT,
		.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
		.storeOp = VK_ATTACHMENT_STORE_OP_STORE,
		.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
		.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
		.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
		.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
	};

	VkAttachmentReference color_attachment_ref = {
		.attachment = 0,
		.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
	};

	VkAttachmentDescription depth_attachment = {
		.format = _depth_image_format,
		.samples = VK_SAMPLE_COUNT_1_BIT,
		.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
		.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
		.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
		.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
		.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
		.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
	};

	VkAttachmentReference depth_attachment_ref = {
		.attachment = 1,
		.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
	};

	VkSubpassDescription subPass = {
		.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
		.colorAttachmentCount = 1,
		.pColorAttachments = &color_attachment_ref,
		.pDepthStencilAttachment = &depth_attachment_ref,
	};

	VkSubpassDependency dependency = {
		.srcSubpass = VK_SUBPASS_EXTERNAL,
		.dstSubpass = 0,
		.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
		.srcAccessMask = 0,
		.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
		.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
	};

	VkAttachmentDescription attachments[2] = { color_attachment, depth_attachment };

	VkRenderPassCreateInfo render_pass_info = {
		.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
		.attachmentCount = 2,
		.pAttachments = attachments,
		.subpassCount = 1,
		.pSubpasses = &subPass,
		.dependencyCount = 1,
		.pDependencies = &dependency,
	};

	return vkCreateRenderPass(*_device, &render_pass_info, NULL, &_render_pass);
}

bool swap_chain_create() {
	const VkDevice* _device = system_bridge_get_device();
	const SurfaceFormats* _supported_surface_formats = system_bridge_get_supported_surface_formats();
	const VkSurfaceCapabilitiesKHR* _surface_capabilities = system_bridge_get_surface_capabilities();
	const VkSurfaceKHR* _surface = system_bridge_get_surface();
	const GLFWwindow* _window = system_bridge_get_window();
	const VkPhysicalDeviceMemoryProperties* _supported_memory_properties = system_bridge_get_supported_memory_properties();

	VkSurfaceFormatKHR picked_surface_format = {
		.format = _supported_surface_formats->data[0].format,
		.colorSpace = _supported_surface_formats->data[0].colorSpace
	};

	glfwGetFramebufferSize(_window, &_swap_chain_image_extent.width, &_swap_chain_image_extent.height);
	glfwWaitEvents();

	VkSwapchainCreateInfoKHR createInfo = {
		.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
		.surface = *_surface,
		.minImageCount = _surface_capabilities->minImageCount,
		.imageFormat = picked_surface_format.format,
		.imageColorSpace = picked_surface_format.colorSpace,
		.imageExtent = _swap_chain_image_extent,
		.imageArrayLayers = 1,
		.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
		.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE,
		.preTransform = _surface_capabilities->currentTransform,
		.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
		.presentMode = VK_PRESENT_MODE_FIFO_KHR,
		.clipped = VK_TRUE,
	};
	PROCESS_VK_RESULT(vkCreateSwapchainKHR(*_device, &createInfo, NULL, &(_swap_chain)));

	vkGetSwapchainImagesKHR(*_device, _swap_chain, &(_swap_chain_images.count), NULL);
	_swap_chain_images.data = malloc(sizeof(VkImage) * _swap_chain_images.count);
	vkGetSwapchainImagesKHR(*_device, _swap_chain, &(_swap_chain_images.count), _swap_chain_images.data);

	_swap_chain_image_format = picked_surface_format.format;
	_swap_chain_image_views.count = _swap_chain_images.count;
	_swap_chain_image_views.data = malloc(sizeof(VkImageView) * _swap_chain_image_views.count);

	for (uint32_t i = 0; i < _swap_chain_images.count; i++)
	{
		VkImageViewCreateInfo swap_chain_image_ci = {
			.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
			.image = _swap_chain_images.data[i],
			.viewType = VK_IMAGE_VIEW_TYPE_2D,
			.format = _swap_chain_image_format,
			.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
			.subresourceRange.baseMipLevel = 0,
			.subresourceRange.levelCount = 1,
			.subresourceRange.baseArrayLayer = 0,
			.subresourceRange.layerCount = 1,
		};
		PROCESS_VK_RESULT(vkCreateImageView(*_device, &swap_chain_image_ci, NULL, _swap_chain_image_views.data + i));
	}

	VkImageCreateInfo depth_image_ci = {
		.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
		.imageType = VK_IMAGE_TYPE_2D,
		.extent.width = _swap_chain_image_extent.width,
		.extent.height = _swap_chain_image_extent.height,
		.extent.depth = 1,
		.mipLevels = 1,
		.arrayLayers = 1,
		.format = _depth_image_format,
		.tiling = VK_IMAGE_TILING_OPTIMAL,
		.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
		.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
		.samples = VK_SAMPLE_COUNT_1_BIT,
		.sharingMode = VK_SHARING_MODE_EXCLUSIVE,
	};

	PROCESS_VK_RESULT(vkCreateImage(*_device, &depth_image_ci, NULL, &_depth_image));

	VkMemoryRequirements memRequirements;
	vkGetImageMemoryRequirements(*_device, _depth_image, &memRequirements);

	VkMemoryAllocateInfo allocInfo = {
		.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
		.allocationSize = memRequirements.size,
	};

	for (uint32_t i = 0; i < _supported_memory_properties->memoryTypeCount; i++)
	{
		if (
			memRequirements.memoryTypeBits & (1 << i) &&
			(_supported_memory_properties->memoryTypes[i].propertyFlags & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT) == VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
		) {
			allocInfo.memoryTypeIndex = i;
		}
	}

	PROCESS_VK_RESULT(vkAllocateMemory(*_device, &allocInfo, NULL, &_depth_image_memory));
	PROCESS_VK_RESULT(vkBindImageMemory(*_device, _depth_image, _depth_image_memory, 0));

	VkImageViewCreateInfo viewInfo = {
		.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
		.image = _depth_image,
		.viewType = VK_IMAGE_VIEW_TYPE_2D,
		.format = _depth_image_format,
		.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT,
		.subresourceRange.baseMipLevel = 0,
		.subresourceRange.levelCount = 1,
		.subresourceRange.baseArrayLayer = 0,
		.subresourceRange.layerCount = 1,
	};
	PROCESS_VK_RESULT(vkCreateImageView(*_device, &viewInfo, NULL, &_depth_image_view));

	PROCESS_VK_RESULT(_create_render_pass(_device));

	_swap_chain_framebuffers.count = _swap_chain_image_views.count;
	_swap_chain_framebuffers.data = malloc(sizeof(SwapChainFramebuffers) * _swap_chain_framebuffers.count);

	for (uint32_t i = 0; i < _swap_chain_framebuffers.count; i++)
	{
		VkImageView attachments[2] = {
			_swap_chain_image_views.data[i],
			_depth_image_view
		};

		VkFramebufferCreateInfo framebufferInfo = {
			.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
			.renderPass = _render_pass,
			.attachmentCount = 2,
			.pAttachments = attachments,
			.width = _swap_chain_image_extent.width,
			.height = _swap_chain_image_extent.height,
			.layers = 1,
		};

		PROCESS_VK_RESULT(vkCreateFramebuffer(*_device, &framebufferInfo, NULL, _swap_chain_framebuffers.data + i));
	}

	return true;
}
void swap_chain_destroy() {
	const VkDevice* _device = system_bridge_get_device();

	for (uint32_t i = 0; i < _swap_chain_framebuffers.count; i += 1)
	{
		vkDestroyFramebuffer(*_device, _swap_chain_framebuffers.data[i], NULL);
	}

	_swap_chain_framebuffers.count = 0;
	free(_swap_chain_framebuffers.data);

	vkDestroyRenderPass(*_device, _render_pass, NULL);
	vkDestroyImageView(*_device, _depth_image_view, NULL);
	vkFreeMemory(*_device, _depth_image_memory, NULL);
	vkDestroyImage(*_device, _depth_image, NULL);

	for (uint32_t i = 0; i < _swap_chain_image_views.count; i += 1)
	{
		vkDestroyImageView(*_device, _swap_chain_image_views.data[i], NULL);
	}

	_swap_chain_image_views.count = 0;
	free(_swap_chain_image_views.data);

	_swap_chain_images.count = 0;
	free(_swap_chain_images.data);

	vkDestroySwapchainKHR(*_device, _swap_chain, NULL);
}

const VkExtent2D* swap_chain_get_image_extent() {
	return &_swap_chain_image_extent;
}
const VkRenderPass* swap_chain_get_render_pass() {
	return &_render_pass;
}
const SwapChainImages* swap_chain_get_images() {
	return &_swap_chain_images;
}
const SwapChainFramebuffers* swap_chain_get_framebuffers() {
	return &_swap_chain_framebuffers;
}
const VkSwapchainKHR* swap_chain_get() {
	return &_swap_chain;
}
